// Copyright (c) 2015 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package auth

const (
	Failure = iota
	Success = iota
)

type Result struct {
	Identity interface{} // identity used for authentication

	code int // authentication result code
}

// NewResult allocates and returns a new Result.
func NewResult(code int, identity interface{}) *Result {
	return &Result{code: code, Identity: identity}
}

// IsValid returns true if authentication result was a success.
func (r *Result) IsValid() bool {
	return r.code == Success
}
