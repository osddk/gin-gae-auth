// Copyright (c) 2015 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package storage

import (
	"bitbucket.org/osddk/gin-gae-session/session"
)

const (
	DefaultNamespace = "gaeauth" // session container namespace
	DefaultKey       = "storage"  // session container key name
)

// SessionStorage is a storage method for Auth identities.
type SessionStorage struct {
	sns *session.Namespace
}

// NewSessionStorage allocates and returns a new SessionStorage.
func NewSessionStorage(s *session.Session) *SessionStorage {
	return &SessionStorage{sns: s.Namespace(DefaultNamespace)}
}

// Clear data from session container.
func (st *SessionStorage) Clear() {
	st.sns.Del(DefaultKey)
}

// Check if session container is empty.
func (st *SessionStorage) IsEmpty() bool {
	v := st.sns.Get(DefaultKey)
	return v == nil
}

// Read loads data from session container.
func (st *SessionStorage) Read() interface{} {
	return st.sns.Get(DefaultKey)
}

// Write stores data in session container.
func (st *SessionStorage) Write(v interface{}) {
	st.sns.Set(DefaultKey, v)
}
